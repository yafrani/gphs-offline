function ttpeval(tree)
%TTPEVAL    Evaluates the tree of a GPLAB HHTTP.
%   TTPEVAL evaluates the tree of a HHTTP problem by calling feval on the
%   tree root, with or without arguments, where arguments are the subtrees.
%
%   Input arguments:
%      TREE - the tree to be evaluated (struct)
%
%   Copyright (C) 2003-2015 Sara Silva (sara@fc.ul.pt)
%   This file is part of the GPLAB Toolbox

if isempty(tree.kids)
    % call function to execute terminal:
    feval(tree.op);
else
    % call function to execute non-terminal with kids as argument:
    feval(tree.op,tree.kids);
end
%fprintf('ntime:%d \n \n', ntime);
