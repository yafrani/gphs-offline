% if no improvement 
function fct_if_improv(actiontruefalse)

global obj_value;
global obj_history;

if size(obj_history,1)>2 && obj_value>obj_history(end-1)
   ttpeval(actiontruefalse{1});
else
   ttpeval(actiontruefalse{2});
end
