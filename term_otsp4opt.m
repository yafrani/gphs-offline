function term_otsp4opt

global instance_name;
global sol_input ;
global sol_output;
global obj_value;
global ntime;
global obj_history;

command=['java -jar ttplab-bbox.jar otsp4opt ' instance_name ' ' sol_input ' ' sol_output];
[status,result]=dos(command);               
teste=sol_input;
arquivo = fopen(teste);
input = fscanf(arquivo,'%c');
fclose(arquivo);
teste=sol_output; %precisa ler o output sem a primeira linha
arquivo = fopen(teste);
output = fscanf(arquivo,'%c');
fclose(arquivo);
[b solution]=strtok(output,'_');
[header b]=strtok(input,'_');
space=find(result==' ');
obj_value=str2double(result(space(1)+1:space(2)-1));
%fprintf('obj_value otsp4opt=%f. \n',obj_value);
obj_history= [obj_history; obj_value];
input=strcat(header,solution);
dlmwrite(sol_input,input,'delimiter','');%write input in txt file
ntime=ntime+1;
