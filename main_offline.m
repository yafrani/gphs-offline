% Hyper heuristic for TTP_problem using GPLAB toolbox.
% training for eil51*, kroA100*, and a280* 
% with C={1,5,10} and F={1,5,10}

clc
clear all
% /!\ we must use > 500000 generations later..
gen = 1000;
% /!\ .. and probably a much larger population too
popsize = 20;

addpath(genpath('./gplab/'));

global sol_input;
global sol_output;
global instances;
global instance_name;


% tunneling files
sol_input = 'sol_input.txt';
sol_output = 'sol_output.txt';



%% training phase
file = 'instances_training.txt';
arquivo = fopen(file);
instances = fscanf(arquivo,'%c');
fclose(arquivo);
instances = strread(instances,'%s');
% train using only one instance
instance_name=char(instances(1));

fprintf('Running HyperGP for TTP problem...');

p = resetparams;
p.sampling = 'tournament';
p.elitism = 'keepbest';
p.survival = 'fixedpopsize';
%p.initpoptype = 'rampedinit';
p.initpoptype = 'fullinit';          % guarantees the initial minimum depth
p = setoperators(p,'crossover',2,2); % p, name, num parents, num childs
p = setfunctions(p, 'fct_con1',1, 'fct_con2',2, 'fct_con3',3, 'fct_con4',4, ...
    'fct_if_local_opt',2, 'fct_if_no_improv',2);
p = setterminals(p, ...
    'term_kpbf', 'term_kpsa', ...
    'term_tsp2opt', ...
    'term_otspswap', 'term_otsp4opt', ...
    'term_okpbf20','term_okpbf30','term_okpbf40' ...
);

p.calcfitness = 'ttpfitness'; % fitness function
p.lowerisbetter = 0;  % 0: maximization 1:minimization
p.autovars = 0;
p.depthnodes = '1';
p.fixedlevel = '1';   % When on, the strict maximum depth of trees is determined by the parameter realmaxlevel
p.dynamiclevel = '0'; % When on, its initial value is determined by the parameter inicdynlevel=8
p.realminlevel=3;     % minimum depth. To activate strictdeph, this filter depthnodes=1, dynamiclevel=1/0, fixedlevel=1(default) manual GPLAB pg26 
p.realmaxlevel=7;     % as the inicmaxlevel
p.tournamentsize = 3; % selecting the best of whole population.


[v,b] = gplabHH(gen,popsize,p);

% save model in mat file
save('gpmodel.mat','v','b');


%% display resulting tree
load('gpmodel-v2.mat')

% draw it
drawtree(v.state.bestsofar.tree);

% print executable string
b.str



%% testing phase

load('gpmodel-v2.mat')

file = 'instances_testing.txt';
arquivo = fopen(file);
instances = fscanf(arquivo,'%c');
fclose(arquivo);
instances = strread(instances,'%s')

nb_inst = size(instances,1);

btree = v.state.bestsofar.tree;
nb_rep = 30;
for i = 1:nb_inst
    for k = 1:nb_rep
        instance_name = char(instances(i));
        ind = ttpfitness(b, v.params, v.data, v.state.terminals, v.state.varsvals);
        fprintf('fitness=%f\n',ind.fitness);
        
        % save output to file
        fileID = fopen('output/results.csv','a');
        fprintf(fileID, '%s %d\n', instance_name, ind.fitness);
        fclose(fileID);
    end
end

