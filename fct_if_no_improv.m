% if no improvement for many iterations
function fct_if_no_improv(actiontruefalse)

global obj_value;
global obj_history;

% if no improvment for 20 iterations
if size(obj_history,1)>20 && obj_value<=max(obj_history(end-20:end-1))
   ttpeval(actiontruefalse{1});
else
   ttpeval(actiontruefalse{2});
end
