% TTPFITNESS   Measures the fitness of a GPLAB HHTTP INDIVIDUAL (tree).
%   TTPFITNESS(INDIVIDUAL,PARAMS,DATA,TERMINALS,VARSVALS) returns
%   the fitness of INDIVIDUAL, measured as the output from a heuritics 
%   sequence (tree str) applied to TTP problem during 'maxtime' time steps. 
%   Also returns other variables as global variables.
%
%   Input arguments:
%      INDIVIDUAL - the individual whose fitness is to measure (struct)
%      PARAMS - the current running parameters (struct)
%      DATA - the dataset on which to measure the fitness (struct)
%      TERMINALS - (not needed here - kept for compatibility purposes)
%      VARSVALS - (not needed here - kept for compatibility purposes)
%   Output arguments:
%      INDIVIDUAL - the individual whose fitness was measured (struct)
function ind = ttpfitness(ind,params,data,terminals,varsvals)

global sol_input; 
global instance_name;

global obj_value;
global obj_history;

obj_value = 0;

folder_name = strtok(char(instance_name), '_');
command=['java -jar ttpapprox.jar database/TTP1_data/' folder_name '-ttp/ ' instance_name ' 71 10000 60000 SOMETHING ' sol_input];
[status res]=dos(command);

ttpeval(ind.tree);

% raw fitness:
ind.fitness = obj_value;
ind.result(1)=ind.fitness;
