# README #

## Git 101 ##

### Set up from scratch ###

```
#!bash

git clone https://<USERNAME>@bitbucket.org/yafrani/hypergpttp.git
```

### Add all files to git for tracking ###

```
#!bash

git add -A
```

### Commit changes (locally) ###

```
#!bash

git commit -am"messages describing the changes made"
```

### pull: download commits (from repo) ###

```
#!bash

git pull origin master
```

### push: upload commits (to repo) ###

```
#!bash

git push origin master
```



## Quick fixes ##

### Fix the java files not running problem ###

Go to File -> project structure -> modules -> then add the src/ folder to Source (choose the src/ folder, then mark as Source)

### Hard reset of last commit ###

1. Listing commits:

```
#!bash
git reflog
```

2. Go back:

```
#!bash
git reset --hard <HASH>
```