function fct_con3(actions)
%prog3    Executes two actions on GP TTP_problem.
%  prog3 executes ACTIONS{1} followed by ACTIONS{2} and ACTIONS{3}. Returns the
%   number of the time step after all actions.
%
%   Input arguments:
%      ACTIONS - three actions to be executed sequentially (cell array)
%

ttpeval(actions{1});
ttpeval(actions{2});
ttpeval(actions{3});
